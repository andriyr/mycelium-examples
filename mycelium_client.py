import threading
from queue import LifoQueue

import grpc

from api import api_pb2_grpc
from api import api_pb2

class MyceliumClient:
    def __init__(self, config):
        self.connected = True
        self.input_queue = LifoQueue()
        self.dopamine_queue = LifoQueue()

        self.stub = api_pb2_grpc.NodeStub(grpc.insecure_channel(config['host'] +':' + str(config['port'])))

        input_stream = threading.Thread(target=self._open_input_stream)
        input_stream.daemon = True
        input_stream.start()

        dopamine_stream = threading.Thread(target=self._open_dopamine_stream)
        dopamine_stream.daemon = True
        dopamine_stream.start()

    def create_neuron_cluster(self, name, size, ratio, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_neuron_cluster_request = api_pb2.CreateNeuronClusterRequest(name=name, size=size, ratio=ratio, graph=graph)
        return self.stub.CreateNeuronCluster(create_neuron_cluster_request)

    def create_synapse_cluster(self, name, neurons_covered, initial_synapse_weight, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_synapse_cluster_request = api_pb2.CreateSynapseClusterRequest(name=name, neuronsCovered=neurons_covered, initialSynapseWeight=initial_synapse_weight, graph=graph)
        return self.stub.CreateSynapseCluster(create_synapse_cluster_request)

    def attach_synapse_cluster(self, synapse_cluster_id, neuron_cluster_id):
        attach_synapse_cluster_request = api_pb2.AttachSynapseClusterRequest(synapseClusterId=synapse_cluster_id, neuronClusterId=neuron_cluster_id)
        return self.stub.AttachSynapseCluster(attach_synapse_cluster_request)

    def create_stdp_preceptor(self, name, max_queue_size, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_stdp_preceptor_request = api_pb2.CreatestdpPreceptorRequest(name=name, maxQueueSize=max_queue_size, graph=graph)
        return self.stub.CreatestdpPreceptor(create_stdp_preceptor_request)

    def attach_stdp_preceptor(self, stdp_preceptor_id, synapse_cluster_id):
        attach_stdp_preceptor_request = api_pb2.AttachstdpPreceptorRequest(STDPPreceptorId=stdp_preceptor_id, synapseClusterId=synapse_cluster_id)
        return self.stub.AttachstdpPreceptor(attach_stdp_preceptor_request)

    def create_modulated_stdp_preceptor(self, name, maxQueueSize, sensitivity, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_modulated_stdp_preceptor_request = api_pb2.CreateModulatedSTDPPreceptorRequest(name=name, maxQueueSize=maxQueueSize, sensitivity=sensitivity, graph=graph)
        return self.stub.CreateModulatedSTDPPreceptor(create_modulated_stdp_preceptor_request)

    def attach_modulated_stdp_preceptor(self, modulated_stdp_preceptor_id, synapse_cluster_id):
        attach_modulated_stdp_preceptor_request = api_pb2.AttachModulatedSTDPPreceptorRequest(modulatedSTDPPreceptorId=modulated_stdp_preceptor_id, synapseClusterId=synapse_cluster_id)
        return self.stub.AttachModulatedSTDPPreceptor(attach_modulated_stdp_preceptor_request)

    def create_dopamine_adapter(self, name, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_dopamine_adapter_request = api_pb2.CreateDopamineAdapterRequest(name=name, graph=graph)
        return self.stub.CreateDopamineAdapter(create_dopamine_adapter_request)

    def attach_dopamine_adapter(self, dopamine_adapter_id, modulated_stdp_preceptor_id):
        attach_dopamine_adapter_request = api_pb2.AttachDopamineAdapterRequest(dopamineAdapterId=dopamine_adapter_id, modulatedSTDPPreceptorId=modulated_stdp_preceptor_id)
        return self.stub.AttachDopamineAdapter(attach_dopamine_adapter_request)

    def create_input_adapter(self, name, size, encoding_window, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_input_adapter_request = api_pb2.CreateInputAdapterRequest(name=name, size=size, encodingWindow=encoding_window, graph=graph)
        return self.stub.CreateInputAdapter(create_input_adapter_request)

    def attach_input_adapter(self, input_adapter_id, neuron_cluster_id):
        attach_input_adapter_request = api_pb2.AttachInputAdapterRequest(inputAdapterId=input_adapter_id, neuronClusterId=neuron_cluster_id)
        return self.stub.AttachInputAdapter(attach_input_adapter_request)

    def create_output_adapter(self, name, size, decoding_window, graph_x, graph_y):
        graph = api_pb2.Graph(x=graph_x, y=graph_y)
        create_output_adapter_request = api_pb2.CreateOutputAdapterRequest(name=name, size=size, decodingWindow=decoding_window, graph=graph)
        return self.stub.CreateOutputAdapter(create_output_adapter_request)

    def attach_output_adapter(self, output_adapter_id, neuron_cluster_id):
        attach_output_adapter_request = api_pb2.AttachOutputAdapterRequest(outputAdapterId=output_adapter_id, neuronClusterId=neuron_cluster_id)
        return self.stub.AttachOutputAdapter(attach_output_adapter_request)

    def stream_input(self, input_adapter_id, values):
        input_message = api_pb2.StreamInputRequest(inputAdapterId=input_adapter_id,  values=values)
        self.input_queue.put(input_message)

    def stream_dopamine(self, dopamine_adapter_id, dopamine):
        dopamine_message = api_pb2.StreamDopamineRequest(dopamineAdapterId=dopamine_adapter_id,  dopamine=dopamine)
        self.dopamine_queue.put(dopamine_message)

    def stream_output(self, output_adapter_id, callback):
        output_message = api_pb2.StreamOutputRequest(outputAdapterId=output_adapter_id)
        for output in self.stub.StreamOutput(output_message):
            callback(output)

    def _open_input_stream(self):
        self.stub.StreamInput(self._input_iterator())

    def _open_dopamine_stream(self):
        self.stub.StreamDopamine(self._dopamine_iterator())

    def _input_iterator(self):
        while self.connected:
            if not self.input_queue.empty():
                yield self.input_queue.get()

    def _dopamine_iterator(self):
        while self.connected:
            if not self.dopamine_queue.empty():
                yield self.dopamine_queue.get()
