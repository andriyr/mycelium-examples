import math
import time
import datetime
import threading

from mycelium_client import MyceliumClient

import matplotlib
import matplotlib.pyplot as pyplot
import matplotlib.animation as animation

config = {
    "host": "localhost",
    "port": 50051
}

DATA_MAX = datetime.timedelta(minutes=2)

client = MyceliumClient(config)

# Generate Graph
neuron_cluster_1 = client.create_neuron_cluster("neuron_cluster_1", 1000, 0.8, 250, 50)

synapse_cluster_1 = client.create_synapse_cluster("synapse_cluster_1", 500, 5, 500, 50)
synapse_cluster_2 = client.create_synapse_cluster("synapse_cluster_2", 500, 5, 500, 200)
synapse_cluster_3 = client.create_synapse_cluster("synapse_cluster_3", 500, 5, 500, 350)

input_adapter_1 = client.create_input_adapter("input_adapter_1", 1, 100, 25, 50)
input_adapter_2 = client.create_input_adapter("input_adapter_2", 1, 100, 25, 250)

output_adapter_1 = client.create_output_adapter("output_adapter_1", 1, 100, 500, 350)

modulated_stdp_preceptor_1 = client.create_modulated_stdp_preceptor("modulated_stdp_preceptor_1", 100, 1, 750, 50)
modulated_stdp_preceptor_2 = client.create_modulated_stdp_preceptor("modulated_stdp_preceptor_2", 100, 1, 750, 200)
modulated_stdp_preceptor_3 = client.create_modulated_stdp_preceptor("modulated_stdp_preceptor_3", 100, 1, 750, 350)

dopamine_adapter_1 = client.create_dopamine_adapter("dopamine_adapter_1", 500, 500)

# attach input adapter 1 & 2 to neuron cluster
client.attach_input_adapter(input_adapter_1.id, neuron_cluster_1.id)
client.attach_input_adapter(input_adapter_2.id, neuron_cluster_1.id)

# attach output adapter to neuron cluster
client.attach_output_adapter(output_adapter_1.id, neuron_cluster_1.id)

# attach synapse cluster to neuron cluster
client.attach_synapse_cluster(synapse_cluster_1.id, neuron_cluster_1.id)
client.attach_synapse_cluster(synapse_cluster_2.id, neuron_cluster_1.id)

# attach modulated_stdp preceptor to synapse cluster
client.attach_modulated_stdp_preceptor(modulated_stdp_preceptor_1.id, synapse_cluster_1.id)
client.attach_modulated_stdp_preceptor(modulated_stdp_preceptor_2.id, synapse_cluster_2.id)
client.attach_modulated_stdp_preceptor(modulated_stdp_preceptor_3.id, synapse_cluster_3.id)

# attach dopamine preceptor to synapse cluster
client.attach_dopamine_adapter(dopamine_adapter_1.id, modulated_stdp_preceptor_1.id)
client.attach_dopamine_adapter(dopamine_adapter_1.id, modulated_stdp_preceptor_2.id)
client.attach_dopamine_adapter(dopamine_adapter_1.id, modulated_stdp_preceptor_3.id)

# Threading lock
lock = threading.Lock()

data = {
    "input_1_y": [],
    "input_2_y": [],
    "target_1_y": [],
    "input_x": [],
    "output_y": [],
    "error_y": [],
    "output_x": []
}

def generate_input(data):
    x = 0
    while True:
        input_1 = (math.sin(x) + 1)/2
        input_2 = (math.cos(x) + 1)/2
        # (cos(2x)-sin(x)^2+cos(x)^3)-sin(x)
        target_1 = ((math.cos(2*x) - math.sin(x)**2 + math.cos(x)**3) - math.sin(x) + 3)/5

        now = datetime.datetime.now()

        lock.acquire()
        data["input_1_y"].append(input_1)
        data["input_2_y"].append(input_2)
        data["target_1_y"].append(target_1)
        data["input_x"].append(now)
        if now - data["input_x"][0] > DATA_MAX:
            data["input_1_y"].pop(0)
            data["input_2_y"].pop(0)
            data["target_1_y"].pop(0)
            data["input_x"].pop(0)
        lock.release()

        x += 0.1
        if x == 360:
            x = 0

        time.sleep(0.1)

def stream_input(data):
    while True:
        lock.acquire()
        input_1 = data["input_1_y"][-1]
        input_2 = data["input_2_y"][-1]
        lock.release()

        client.stream_input(input_adapter_1.id, [input_1])
        client.stream_input(input_adapter_2.id, [input_2])

        time.sleep(0.1)

def output_callback(message):
    if message.values[0] != 0:
        error = data["target_1_y"][-1] - message.values[0]

        now = datetime.datetime.now()

        lock.acquire()
        data["output_y"].append(message.values[0])
        data["error_y"].append(error)
        data["output_x"].append(now)
        if now - data["output_x"][0] > DATA_MAX:
            data["output_y"].pop(0)
            data["error_y"].pop(0)
            data["output_x"].pop(0)
        lock.release()

        client.stream_dopamine(dopamine_adapter_1.id, error)

def animate(i, data, figure, subplots):
    lock.acquire()
    input_1_y = data["input_1_y"].copy()
    input_2_y = data["input_2_y"].copy()
    target_1_y = data["target_1_y"].copy()
    input_x = data["input_x"].copy()
    output_y = data["output_y"].copy()
    error_y = data["error_y"].copy()
    output_x = data["output_x"].copy()
    lock.release()

    subplots[0].clear()
    subplots[0].plot(input_x, input_1_y)
    subplots[1].clear()
    subplots[1].plot(input_x, input_2_y)
    subplots[2].clear()
    subplots[2].plot(input_x, target_1_y)
    subplots[3].clear()
    subplots[3].plot(output_x, output_y)
    subplots[4].clear()
    subplots[4].plot(output_x, error_y)

# Generate Plot
def plot(data):
    figure, subplots = pyplot.subplots(5, 1)  # a figure with a 2x2 grid of Axes
    figure.suptitle('Wave transformation')  # Add a title so we know which it is

    subplots[0].set_title('input_1')
    subplots[0].set_xlabel('Time')
    subplots[0].set_ylabel('Amplitude')
    subplots[1].set_title('input_2')
    subplots[1].set_xlabel('Time')
    subplots[1].set_ylabel('Amplitude')
    subplots[2].set_title('target')
    subplots[2].set_xlabel('Time')
    subplots[2].set_ylabel('Amplitude')
    subplots[3].set_title('output')
    subplots[3].set_xlabel('Time')
    subplots[3].set_ylabel('Amplitude')
    subplots[4].set_title('error')
    subplots[4].set_xlabel('Time')
    subplots[4].set_ylabel('Amplitude')

    while True:
        ani = animation.FuncAnimation(figure, animate, fargs=(data, figure, subplots,))
        pyplot.show()


# RUN
generate_input_thread = threading.Thread(target=generate_input, args=(data,))
generate_input_thread.daemon = True
generate_input_thread.start()

stream_input_thread = threading.Thread(target=stream_input, args=(data,))
stream_input_thread.daemon = True
stream_input_thread.start()

plot_thread = threading.Thread(target=plot, args=(data,))
plot_thread.daemon = True
plot_thread.start()

client.stream_output(output_adapter_1.id, output_callback)

while True:
    time.sleep(10)
    continue
