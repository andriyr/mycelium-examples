# mycelium-examples

Spiking neural network written in golang and based on the Izhikevich spiking neural network model.

## Getting Started

1. Checkout the project and the dependent submodules with the following command:

        git clone --recursive git@gitlab.com:andriyr/mycelium-app.git

2. Generete the python grpc files:

    pip3 install grpcio-tools
    pip3 install googleapis-common-protos

    Run this in the mycelium proto directory:

    python3 -m grpc_tools.protoc -I./ --python_out=. --grpc_python_out=. api.proto

3. Add proto files to pythonpath

    export PYTHONPATH=${PYTHONPATH}:/home/andriy/Documents/mycelium-examples/api
