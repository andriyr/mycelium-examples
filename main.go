// Streaming test, feeds data to all input adapters

package main

import (
    "log"
    "time"
    "math/rand"

    "../api"

    "golang.org/x/net/context"
    "google.golang.org/grpc"
)

func getInputAdapters(client api.NodeClient) ([]*api.InputAdapterResponse) {
    context, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()

    node, err := client.Node(context, &api.NodeRequest{})
    log.Println(err)
    return node.GetInputAdapters()
}

func streamInputNoise(client api.NodeClient, inputAdapters []*api.InputAdapterResponse) {
    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()
    stream, _ := client.StreamInput(ctx)

    for _, inputAdapter := range inputAdapters {
        stream.Send(&api.StreamInputRequest{
            InputAdapterId: inputAdapter.GetId(),
            Values: generateRandomArray(int(inputAdapter.GetSize())),
        })
        time.Sleep(time.Millisecond * 10)
    }
}

func generateRandomArray(size int) []float64 {
    input := make([]float64, size)
    for index,_ := range input {
        input[index] = rand.Float64()
    }
    return input
}

func main() {
    // Conenct to server
    conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()

    client := api.NewNodeClient(conn)

    // Get input adapters
    inputAdapters := getInputAdapters(client)
    //
    log.Println(inputAdapters)

    for {
        streamInputNoise(client, inputAdapters)
        time.Sleep(10 * time.Millisecond)
    }
}
